
//index.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util.js');

Page({
  data: {
    userInfo: {},
    title1: 'PDF转换',
    title2: '其他',
    aboveBtn: [
      {
        name: 'PDF转Word',
        url: '/pdf/word',
        suffix: 'doc'
      }, {
        name: 'word,excel,ppt,图片转PDF',
        url: '/file/pdf',
        suffix: 'pdf'
      }, {
        name: 'PDF转图片',
        url: '/pdf/img',
        suffix: 'png'
      }, {
        name: '图片转PDF',
        url: '/img/pdf',
        suffix: 'pdf'
      }, {
        name: 'PDF转PPT',
        url: '/pdf/excel',
        suffix: 'ppt'
      }, {
        name: 'PDF转Text',
        url: '/pdf/text',
        suffix: 'txt'
      },
    ]
  },

  onLoad: function () {

  },

  onShow() {

  },


  chooseFile: function (event) {
    // 获取按钮绑定的值，即上传 URL
    const uploadUrl = event.currentTarget.dataset.url;
    const suffix = event.currentTarget.dataset.suffix;
    wx.chooseMessageFile({
      count: 1,
      type: 'file',
      success: function (res) {
        const file = res.tempFiles[0];
        // 上传文件
        wx.uploadFile({
          url: app.globalData.apiDomain + uploadUrl,
          filePath: file.path,
          name: 'file',
          formData: {
            imageFormat: 'png'
          },
          header: {
            'Content-Type': 'multipart/form-data'
          },
          responseType: "arraybuffer",
          success: function (res) {
            if (res.statusCode === 200) {
              // 将二进制数据保存到本地文件
              wx.getFileSystemManager().writeFile({
                filePath: wx.env.USER_DATA_PATH + '/downloadedFile' + suffix, // 保存的文件路径，自行替换
                data: res.data,
                encoding: 'binary',
                success: function (writeRes) {
                  console.log('文件保存成功', writeRes);
                },
                fail: function (writeError) {
                  console.error('文件保存失败', writeError);
                }
              });
            } else {
              console.error('文件下载失败', res);
            }
            /* // 下载文件
            wx.downloadFile({
              url: fileUrl,
              success: function (downloadRes) {
                // 下载成功后保存文件
                if (downloadRes.statusCode === 200) {
                  wx.saveFile({
                    tempFilePath: downloadRes.tempFilePath,
                    success: function (saveRes) {
                      console.log('文件保存成功', saveRes.savedFilePath);
                      // 在这里可以根据需要进行其他操作
                    },
                    fail: function (saveError) {
                      console.error('文件保存失败', saveError);
                    }
                  });
                } else {
                  console.error('文件下载失败', downloadRes);
                }
              },
              fail: function (downloadError) {
                console.error('文件下载失败', downloadError);
              }
            }); */
          },
          fail: function (uploadError) {
            console.error('上传失败', uploadError);
          }
        });
      }
    });
  }
})
